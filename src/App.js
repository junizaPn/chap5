import { View, Text } from 'react-native'
import React from 'react'
import Login from './LoginScreen'

const App = () => {
  return (
    <View>
      <Login />
    </View>
  )
}

export default App