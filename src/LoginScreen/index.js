import { Text, View, Image, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import React, { useState, useEffect } from 'react';
import styles from './styles';

export default function Login() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  
  return (
    <View style={styles.ContainerLoginComponent}>
      <View style={styles.containerHeader}>
        <Text style={styles.textHeader}>WELCOME BACK</Text>
        <Text style={styles.textTitle}>Login to continue</Text>
      </View>
      <View style={styles.containerInput}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => setEmail(text)}
          value={email}
          placeholder="Email"
          placeholderTextColor="black"
        />
        <TextInput
          style={styles.textInput}
          onChangeText={text => setPassword(text)}
          value={password}
          placeholder="Password"
          placeholderTextColor="black"
          secureTextEntry={true}
        />
        <TouchableOpacity style={styles.buttonLogin} >
          <Text style={styles.textLogin}>Login</Text>
        </TouchableOpacity>
        <Text style={styles.textAccount}>Don't have an account?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')} >
          <Text style={styles.textRegister}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

