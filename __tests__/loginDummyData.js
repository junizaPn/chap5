export const dataLogin = {
    "user": {
        "role": "user",
        "isEmailVerified": true,
        "email": "saysay@gmail.com",
        "name": "sayjuniza",
        "id": "62511a3822536c7fc70023e4"
    },
    "tokens": {
        "access": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjUxMWEzODIyNTM2YzdmYzcwMDIzZTQiLCJpYXQiOjE2NDk4NjQzOTQsImV4cCI6MTY0OTg2NjE5NCwidHlwZSI6ImFjY2VzcyJ9.y_WXiiAO8CT_UP31r_gA6N5i_f0NwXVpk2tj4XhqQP4",
            "expires": "2022-04-13T16:09:54.294Z"
        },
        "refresh": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjUxMWEzODIyNTM2YzdmYzcwMDIzZTQiLCJpYXQiOjE2NDk4NjQzOTQsImV4cCI6MTY1MjQ1NjM5NCwidHlwZSI6InJlZnJlc2gifQ.cKYdt5ustcJG3j2-cAia2PmPYwqY61x3X_hRzEpEwBs",
            "expires": "2022-05-13T15:39:54.295Z"
        }
    }
}