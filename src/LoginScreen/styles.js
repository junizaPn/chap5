import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    ContainerLoginComponent: {
        backgroundColor: '#4d6000',
        width: 415,
        height: 1280
    },
    containerHeader:{
        alignItems: 'center',
    },
    textHeader: {
        fontSize: 30,
        fontWeight: 'bold',
        marginHorizontal: 5,
        marginTop: 20,
        color: 'white',
        textAlign: 'center',
    },
    logome:{
        width: 280,
        height: 280,
        alignContent: 'center',
        marginTop: 20
    },
    textTitle:{
        fontSize: 20,
        marginHorizontal: 5,
        color: 'white',
        textAlign: 'center',
    },
    containerInput:{
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    textInput:{
        backgroundColor: 'white',
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 12,
        padding: 14,
        marginVertical: 15,
        marginHorizontal: 15,
        fontSize: 18
        
    },
    buttonLogin:{
        backgroundColor: '#c9e265',
        borderColor: '#c9e265',
        borderWidth: 3,
        borderRadius: 25,
        padding: 6,
        marginVertical: 15,
        marginHorizontal: 120,
        
    },
    textLogin:{
        color: 'black',
        fontWeight: 'bold',
        fontSize: 22,
        alignSelf: 'center',
    },
    textAccount:{
        fontSize: 18,
        alignSelf: 'center',
    },
    textRegister:{
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25,
        alignSelf: 'center',
        marginTop: 12
    }
})
export default styles;