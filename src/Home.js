import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

export default class Home extends React.Component {
    render() {
        return (
            <View style={styles.text}>
                <Text style={{fontSize: 25}}>Home Component</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text:{
        flex: 1, alignItems: 'center', justifyContent: 'center'
    }
})